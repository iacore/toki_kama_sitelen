Some sort of programming language.

***

This project is developed with [Nimskull](https://github.com/nim-works/nimskull). To build the compiler, clone the repository and run `./koch.py boot -d:release -d:nimDebugUtils --stacktrace -d:panic=on` inside. The compiler will be at `bin/nim`.

Nim 2.0 works too, apparently.

`parser.nim` parser & interpreter
`program_2x2` the 2x2 2048 program

To run the game with verbose logging, use `nim r parser.nim`.  
To build the game with verbose logging, use `nim c parser.nim`.  
To play the game, use `nim r -d:release parser.nim`.  

## Language Design

`toki a` marks the start of a line comment.

There is only one type of varible: the toki pona noun (e.g. `toki`, `toki pona`). This is called a symbol, or `Sym` in code.

Each symbol has a value, which is also a symbol. By default, every symbol has the value ``.

`sama ala sama` sets a global compare flag.

## Grammatic Critic From vierkantor

overuse of "pi" as noun groupings. nimi X.. would be arguably better.
"pi" is a direct match to "of", the function is regrouping: much more like the $ operator in Haskell

idea: multiple verb groups: mi li ijo tonsi li soweli ko a
en is for joining noun phrases, if you want multiple verb phrases you can just put them next to each other.

