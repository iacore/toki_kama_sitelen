import std/strutils
import std/sugar
import std/sequtils

template debugOnlyEcho*(x: varargs[untyped]) =
    if not defined(release):
        debugEcho(x)

type PartOfSpeechType = enum
    synLit
    synNoun
    synRest

type PartOfSpeech = object
    case part: PartOfSpeechType
    of synLit:
        lit: string
    of synNoun:
        noun: ptr seq[string]
    of synRest:
        rest: ptr seq[string]

proc l(s: string): PartOfSpeech = PartOfSpeech(part: synLit, lit: s)
proc noun(s: var seq[string]): PartOfSpeech = PartOfSpeech(part: synNoun, noun: s.addr)
proc toend(s: var seq[string]): PartOfSpeech = PartOfSpeech(part: synRest, rest: s.addr)

proc is_sentinel(token: string): bool = ["li", "tawa", "en"].contains(token)

proc match(stmt: openArray[string], parts: openArray[PartOfSpeech]): bool =
    if parts.len == 0 and stmt.len == 0:
        return true
    if parts.len == 0 or stmt.len == 0:
        return false
    case parts[0].part
    of synLit:
        if stmt[0] == parts[0].lit:
            return match(stmt[1..^1], parts[1..^1])
        else:
            return false
    of synNoun:
        var i = 0
        while i < stmt.len:
            if stmt[i].is_sentinel:
                break
            i += 1
        if i == 0: return false
        parts[0].noun[] = stmt[0..<i]
        return match(stmt[i..^1], parts[1..^1])
    of synRest:
        parts[0].rest[] = stmt[0..^1]
        return match([], parts[1..^1])

var instructions: seq[seq[string]] = @[]

for line in open("program_2x2").lines:
    var tokens = line.split.filter(a => a.len != 0)
    for i in 0..<tokens.len-1:
        if tokens[i..i+1] == ["toki", "a"]:
            tokens = tokens[0..<i]
            break
    if tokens.len != 0:
        instructions.add tokens

type Sym = seq[string]
type ValueKind = enum
    quoted
    unquote
    direct
type Value = object
    case kind: ValueKind
    of quoted:
        quoted: Sym
    of unquote:
        unquote: Sym
    of direct:
        direct: Sym

# func `==`(x: Value, y: Value): bool =
#     x.kind == y.kind and (case x.kind
#     of quoted:
#         x.quoted == y.quoted
#     of unquote:
#         x.unquote == y.unquote
#     of direct:
#         x.direct == y.direct)


template todoAssert(x: varargs[untyped]) =
    doAssert(x)

proc split(sym: Sym, sep: string): seq[seq[string]] =
    var acc: seq[string]
    template flush() =
        result.add(acc)
        reset(acc)
    for token in sym:
        if token == sep:
            flush()
        else:
            acc.add(token)
    flush()

func parse(s: seq[string]): Value =
    if s.len >= 2 and s[0..<2] == ["nimi", "pi"]:
        Value(kind: quoted, quoted: s[2..^1])
    elif s.len >= 2 and s[0..<2] == ["insa", "pi"]:
        Value(kind: unquote, unquote: s[2..^1])
    else:
        Value(kind: direct, direct: s)

# echo parse(@["ijo", "wile", "en", "nimi", "pi", "ijo", "Alfa"])
# quit 0

type InstructionType = enum
    print_env
    var_set
    var_set_concat
    fn_start
    syscall
    goto
    compare
    if_equal
    if_unequal

type Instruction = object
    case op: InstructionType
    of print_env:
        discard
    of var_set:
        dest: Value
        value: Value
    of var_set_concat:
        dest0: Value
        value0: Value
        value1: Value
    of fn_start:
        fn_name: Sym
    of syscall:
        arg: Value
        target: Sym
    of goto:
        where: Sym
    of compare:
        lhs: Value
        rhs: Value
    of if_equal, if_unequal:
        then: ref Instruction

proc new[T](x: T): ref T =
    let then = T.new
    then[] = x
    then

proc parse_inst(stmt: seq[string]): Instruction =
    var a, b, c: seq[string]
    if match(stmt, [l"ma",l"li",l"seme"]):
        Instruction(op: print_env)
    elif match(stmt, [l"mi", l"toki", l"e", noun(a), l"tawa", noun(b)]):
        Instruction(op: syscall, arg: parse(a), target: b)
    elif match(stmt, [l"mi", l"tawa", l"e", noun(a)]):
        Instruction(op: goto, where: a)
    elif match(stmt, [l"sama", l"la", toend(a)]):
        Instruction(op: if_equal, then: parse_inst(a).new)
    elif match(stmt, [l"sama", l"ala", l"la", toend(a)]):
        Instruction(op: if_unequal, then: parse_inst(a).new)
    elif match(stmt, [l"mi", noun(a)]):
        Instruction(op: fn_start, fn_name: a)
    elif match(stmt, [noun(a), l"li", l"sama", l"ala", l"sama", noun(b)]):
        Instruction(op: compare, lhs: parse(a), rhs: parse(b))
    elif match(stmt, [noun(a), l"li", noun(b), l"en", noun(c)]):
        let dest =
            if a.len>=2 and a[0..<2] == ["insa", "pi"]:
                Value(kind: direct, direct: a[2..^1])
            else:
                Value(kind: quoted, quoted: a)
        Instruction(op: var_set_concat, dest0: dest, value0: parse(b), value1: parse(c))
    elif match(stmt, [noun(a), l"li", noun(b)]):
        let dest =
            if a.len>=2 and a[0..<2] == ["insa", "pi"]:
                Value(kind: direct, direct: a[2..^1])
            else:
                Value(kind: quoted, quoted: a)
        Instruction(op: var_set, dest: dest, value: parse(b))
    else:
        raiseAssert "unknown syntax: " & stmt.join(" ")

var insts: seq[Instruction]

for stmt in instructions:
    insts.add(parse_inst(stmt))

import std/enumerate
import std/tables

var labels: Table[Sym, int]

for i, inst in enumerate(insts):
    if inst.op == fn_start:
        labels[inst.fn_name] = i


var env: Table[Sym, Sym]

proc lookup(x: Value): Sym =
    case x.kind
    of quoted:
        x.quoted
    of direct:
        if x.direct in env:
            env[x.direct]
        else:
            @[]
    of unquote:
        let l0 =
            if x.unquote in env:
                env[x.unquote]
            else:
                @[]
        if l0 in env:
            env[l0]
        else:
            @[]

import std/random
import std/terminal

proc outa(s: string) = stdout.write(s)

proc extcall(target: Sym, arg: Value) =
    if target == ["ma"]:
        case arg.kind
        of quoted:
            todoAssert arg.quoted.len == 1
            case arg.quoted[0]
            of "ala": outa "\t|\t"
            of "n": outa "\n"
            of "a": outa "a"
            else:
                outa arg.quoted[0]
        of direct:
            outa arg.lookup.join(" ")
        of unquote:
            raiseAssert "no"
    elif target == ["nasin", "seme"]:
        while true:
            let ch = getch()
            case ch
            of 'q', char(3):
                quit 0
            of '\x1b':
                let ch1 = getch()
                let ch2 = getch()
                if ch1 == '[':
                    let keyname =
                        case ch2
                        of 'A': "Up"
                        of 'B': "Down"
                        of 'C': "Right"
                        of 'D': "Left"
                        else:
                            continue
                    env[@["ijo", "sin"]] = @[keyname]
                    break
            else:
                debugOnlyEcho "unknown ch: ", int(ch)
    elif target == ["nanpa", "nasa"]:
        let arg2 = arg.lookup
        # debugOnlyEcho "arg", $arg & " " & $arg2
        let choices = arg2.split("en")
        env[@["ijo", "sin"]] = sample(choices)
    else:
        debugOnlyEcho "Unhandled syscall", target, arg
        quit 1

var flag_equal = false

var rip = 0

proc run_inst(inst: Instruction) =
    case inst.op
    of print_env:
        echo "rip=", rip, "  ", instructions[rip].join(" ")
        echo "env= ", env
    of var_set:
        debugOnlyEcho inst.dest.lookup, "=", inst.value.lookup
        env[inst.dest.lookup] = inst.value.lookup
    of var_set_concat:
        let val0 = inst.value0.lookup
        let val1 = inst.value1.lookup
        env[inst.dest0.lookup] =
            if val0.len == 0:
                val1
            elif val1.len == 0:
                val0
            else:
                val0 & @["en"] & val1
    of fn_start:
        discard
    of syscall:
        extcall(inst.target, inst.arg)
    of goto:
        if inst.where notin labels:
            rip = -2
        else:
            rip = labels[inst.where] - 1
    of compare:
        # debugOnlyEcho "compare"
        # debugOnlyEcho inst.lhs, inst.rhs
        # debugOnlyEcho inst.lhs.lookup, inst.rhs.lookup
        flag_equal = inst.lhs.lookup == inst.rhs.lookup
        # debugOnlyEcho flag_equal
    of if_equal:
        debugOnlyEcho "if_equal", flag_equal, inst.then[]
        if flag_equal:
            run_inst(inst.then[])
    of if_unequal:
        if flag_equal.not:
            run_inst(inst.then[])

while rip < insts.len:
    debugOnlyEcho "rip=", rip, "  ", instructions[rip].join(" ")
    run_inst(insts[rip])
    rip += 1


# mi Noun li Noun
# mi toki e Noun tawa Noun
# mi Noun
# mi tawa e Noun
